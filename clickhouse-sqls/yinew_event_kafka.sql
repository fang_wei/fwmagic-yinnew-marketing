-- 设置允许使用map类型的数据结构
set allow_experimental_map_type = 1;

-- 创建一张普通的MergeTree表
create table default.yinew_event_detail
(
    account        String,
    appId          String,
    appVersion     String,
    carrier        String,
    deviceId       String,
    deviceType     String,
    eventId        String,
    ip             String,
    latitude       Float64,
    longitude      Float64,
    netType        String,
    osName         String,
    osVersion      String,
    properties     Map(String,String),
    releaseChannel String,
    resolution     String,
    sessionId      String,
    timeStamp      Int64,
    INDEX u (deviceId) TYPE minmax GRANULARITY 3,
    INDEX t (timeStamp) TYPE minmax GRANULARITY 3
) ENGINE = MergeTree()
      ORDER BY (deviceId, timeStamp);

-- 创建一张Kafka引擎的表，用于获取最新的Kafka数据
drop table default.yinew_event_detail_kafka;
create table default.yinew_event_detail_kafka
(
    account        String,
    appId          String,
    appVersion     String,
    carrier        String,
    deviceId       String,
    deviceType     String,
    eventId        String,
    ip             String,
    latitude       Float64,
    longitude      Float64,
    netType        String,
    osName         String,
    osVersion      String,
    properties     Map(String,String),
    releaseChannel String,
    resolution     String,
    sessionId      String,
    timeStamp      Int64
) ENGINE = Kafka('hd1:9092,hd2:9092,hd3:9092', 'yinew_applog', 'group1', 'JSONEachRow');

-- 创建一张物化视图表，将Kafka表关联到事件明细表
create MATERIALIZED VIEW yinew_event_view TO yinew_event_detail
as
select account,
       appId,
       appVersion,
       carrier,
       deviceId,
       deviceType,
       eventId,
       ip,
       latitude,
       longitude,
       netType,
       osName,
       osVersion,
       properties,
       releaseChannel,
       resolution,
       sessionId,
       timeStamp
from yinew_event_detail_kafka;
