<#list events as event>
    select
    deviceId,
    count(1) as cnt
    from yinew_event_detail
    where
    deviceId=?
    and eventId='${event.eventId}'
    <#list event.properties?keys as key>
        and properties['${key}'] = '${event.properties[key]}'
    </#list>
    and timeStamp between ? and ?
    group by deviceId;
</#list>