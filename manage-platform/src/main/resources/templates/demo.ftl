<html>
<head>
    <title>Welcome</title>
</head>
<body>
<h1>Welcome ${user} !</h1>
<p>We have these animals:</p>
<ul>
    <#list animals as animal>
        <li>${animal.name} for ${animal.price} ¥</li>
    </#list>
</ul>
</body>
</html>