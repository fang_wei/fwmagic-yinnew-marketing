package com.fwmagic.drools.ftl.manageplatform.controller;

import com.fwmagic.drools.ftl.manageplatform.Animal;
import com.fwmagic.drools.ftl.manageplatform.bean.RuleAtomicParam;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class DemoController {

    @RequestMapping("/demo")
    public String demo(String user) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("user", user);

        List<Animal> animals = new ArrayList<>();
        Animal a1 = new Animal("dog", new BigDecimal(100.0));
        Animal a2 = new Animal("cat", new BigDecimal(200.0));
        Animal a3 = new Animal("pig", new BigDecimal(300.0));
        animals.add(a1);
        animals.add(a2);
        animals.add(a3);
        map.put("animals", animals);

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_22);
        cfg.setDirectoryForTemplateLoading(new File("manage-platform/src/main/resources/templates"));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        Template template = cfg.getTemplate("demo.ftl");
//        OutputStreamWriter out = new OutputStreamWriter(System.out);
        StringWriter out = new StringWriter();

        template.process(map, out);
        StringBuffer buffer = out.getBuffer();
        System.err.println(buffer.toString());
        return "ok";
    }

    @RequestMapping("/cntsql")
    public String getRuleCountSql() throws Exception{
        ArrayList<RuleAtomicParam> countParams = new ArrayList<>();

        RuleAtomicParam rap1 = new RuleAtomicParam();
        rap1.setEventId("E");
        HashMap<String, String> prop1 = new HashMap<>();
        prop1.put("p1","v2");
        prop1.put("p3","v4");
        rap1.setProperties(prop1);

        RuleAtomicParam rap2 = new RuleAtomicParam();
        rap2.setEventId("F");
        HashMap<String, String> prop2 = new HashMap<>();
        prop2.put("p5","v6");
        prop2.put("p7","v8");
        rap2.setProperties(prop2);

        countParams.add(rap1);
        countParams.add(rap2);

        HashMap<String, Object> map = new HashMap<>();
        map.put("events",countParams);

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_22);
        cfg.setDirectoryForTemplateLoading(new File("manage-platform/src/main/resources/templates"));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        Template template = cfg.getTemplate("cntsql.ftl");
        StringWriter out = new StringWriter();

        template.process(map, out);
        StringBuffer buffer = out.getBuffer();
        System.err.println(buffer.toString());
        return "ok";
    }
}
