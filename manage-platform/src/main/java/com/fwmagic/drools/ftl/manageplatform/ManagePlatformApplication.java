package com.fwmagic.drools.ftl.manageplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagePlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagePlatformApplication.class, args);
    }

}
