package com.fwmagic.drools.ftl.manageplatform;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Animal {
    private String name;
    private BigDecimal price;
}
