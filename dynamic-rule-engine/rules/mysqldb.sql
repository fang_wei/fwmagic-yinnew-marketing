CREATE TABLE `t_canal_rules`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT,
    `rule_name`    varchar(255) DEFAULT NULL,
    `rule_code`    varchar(4096) DEFAULT NULL,
    `rule_status`  int(255) DEFAULT NULL,
    `rule_type`    varchar(255) DEFAULT NULL,
    `rule_version` int(255) DEFAULT NULL,
    `cnt_sqls`     varchar(4096) DEFAULT NULL,
    `seq_sqls`     varchar(4096) DEFAULT NULL,
    `rule_creator` varchar(255) DEFAULT NULL,
    `rule_auditor` varchar(255) DEFAULT NULL,
    `create_time`  datetime DEFAULT NULL,
    `update_time`  datetime DEFAULT NULL,
    PRIMARY        KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;