select deviceId,
       sequenceMatch('.*(?1).*(?2).*')(
                     toDateTime(`timeStamp`),
                     eventId = 'A' and properties['p3'] = 'v2',
                     eventId = 'C' and properties['p6'] = 'v2'
           ) as isMatch2,
       sequenceMatch('.*(?1).*')(
                     toDateTime(`timeStamp`),
                     eventId = 'A' and properties['p3'] = 'v2',
                     eventId = 'C' and properties['p6'] = 'v2'
           ) as isMatch1
from yinew_event_detail
where deviceId = ?
  and timeStamp between ? and ?
  and ((eventId = 'A' and properties['p3'] = 'v2') or (eventId = 'C' and properties['p6'] = 'v2'))
group by deviceId;
