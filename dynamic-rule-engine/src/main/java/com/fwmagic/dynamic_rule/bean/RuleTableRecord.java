package com.fwmagic.dynamic_rule.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RuleTableRecord {

    //自增序号
    private int id;

    //规则名称
    private String rule_name;

    //规则代码
    private String rule_code;

    //规则状态
    private int rule_status;

    //规则类型
    private String rule_type;

    //规则版本
    private String rule_version;

    //count查询的sql
    private String cnt_sqls;

    //序列查询的sql
    private String seq_sqls;
}
