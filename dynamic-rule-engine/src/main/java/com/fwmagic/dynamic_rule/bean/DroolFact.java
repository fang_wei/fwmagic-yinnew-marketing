package com.fwmagic.dynamic_rule.bean;

import com.fwmagic.dynamic_rule.service.QueryRouterServiceV4;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DroolFact {
    private LogBean logBean;

    private RuleParam ruleParam;

    private QueryRouterServiceV4 queryRouterV4;

    private boolean match;

}
