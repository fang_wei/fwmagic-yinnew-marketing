package com.fwmagic.dynamic_rule.bean;


import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 原子事件参数
 */
@Data
public class RuleAtomicParam implements Serializable {
    //事件的类型要求
    private String eventId;

    //事件的属性要求
    private HashMap<String, String> properties;

    //条件要求的阈值
    private int cnt;

    //服务查询到实际值
    private int realCnt;

    //要求的事件发生时间起始
    private long rangeStart;

    //要求的事件发生的结束
    private long rangeEnd;

    //根据事件属性和阈值自动生成的查询clickhouse的sql
    private String actionCountQuerySql;

    //用于记录初始开始时间
    private long originRangeStart;

    //用于记录初始结束时间
    private long originRangeEnd;

    public void setOriginRangeStart(long originRangeStart) {
        this.originRangeStart = originRangeStart;
        this.rangeStart = originRangeStart;
    }

    public void setOriginRangeEnd(long originRangeEnd) {
        this.originRangeEnd = originRangeEnd;
        this.rangeEnd = originRangeEnd;
    }
}
