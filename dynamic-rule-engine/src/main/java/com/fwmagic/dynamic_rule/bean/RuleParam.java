package com.fwmagic.dynamic_rule.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleParam implements Serializable {
    //规则名称标识
    private String ruleId;
    //规则名称
    private String ruleName;
    //规则中的触发条件
    private RuleAtomicParam triggerParam;

    //规则中的用户画像条件
    private HashMap<String, String> userProfileParam;

    //规则中的行为次数条件
    private List<RuleAtomicParam> userActionCountParam;

    //规则中的行为次序条件
    private List<RuleAtomicParam> userActionSequenceParam;

    //用于记录查询服务所返回的序列中匹配的最大步骤数
    private int userActionSequenceQueriedMaxStep;

    //根据规则中的行为次序条件自动生成的查询clickhouse的sql
    private String actionSequenceQuerySql;

}
