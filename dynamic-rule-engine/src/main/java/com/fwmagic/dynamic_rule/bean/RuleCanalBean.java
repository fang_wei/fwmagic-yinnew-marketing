package com.fwmagic.dynamic_rule.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * 用于抽取Cannal发送到kafka中的json数据，其他字段忽略，目前只关心这两个字段
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleCanalBean {
    private List<RuleTableRecord> data;

    private String type;
}
