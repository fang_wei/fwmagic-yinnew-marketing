package com.fwmagic.dynamic_rule.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultBean {
    private String ruleId;
    private String deviceId;
    private long timeStamp;
}
