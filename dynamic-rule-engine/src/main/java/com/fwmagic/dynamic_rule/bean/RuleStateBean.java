package com.fwmagic.dynamic_rule.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.kie.api.runtime.KieSession;

/**
 * 用于封装放入state中的规则相关信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleStateBean {

    //规则名称
    private String ruleName;

    //用于将事件放入规则中
    private KieSession kieSession;

    //规则参数
    private RuleParam ruleParam;

    //规则类型
    private String ruleType;

    //规则类calss文件
    private String routerClass;

    //count条件sql,多个sql用分号";"分割
    private String cntSqls;

    //sequence条件sql
    private String seqSqls;

}
