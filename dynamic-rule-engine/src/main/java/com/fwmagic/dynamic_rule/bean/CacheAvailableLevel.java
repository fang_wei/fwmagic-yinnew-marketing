package com.fwmagic.dynamic_rule.bean;

public enum CacheAvailableLevel {

    //缓存结果全部有效
    WHOLE_AVL,

    //缓存结果部分有效
    PARTIAL_AVL,

    //缓存结果无效
    UN_AVL

}
