package com.fwmagic.dynamic_rule.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CacheResult {

    //缓存结果的key
    private String key;

    //缓存结果的value
    private Integer value;

    //缓存数据的时间窗口开始
    private Long timeStart;

    //缓存数据的时间窗口结束
    private Long timeEnd;

    //缓存结果的有效等级
    private CacheAvailableLevel availableLevel;

}
