package com.fwmagic.dynamic_rule.datagen;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * 读取drl文件写入Mysql中
 */
public class ReadFile2Mysql4Drools2 {
    public static void main(String[] args) throws Exception {
        //读取文件中的规则
        String rule = FileUtils.readFileToString(new File("/Users/fangwei/workspace/self/fwmagic-yinnew-marketing/dynamic-rule-engine/rules/rule1/rule1.drl"), "UTF-8");
        String cntSql = FileUtils.readFileToString(new File("/Users/fangwei/workspace/self/fwmagic-yinnew-marketing/dynamic-rule-engine/rules/rule1/rule1_cnt.sql"), "UTF-8");
        String seqSql = FileUtils.readFileToString(new File("/Users/fangwei/workspace/self/fwmagic-yinnew-marketing/dynamic-rule-engine/rules/rule1/rule1_seq.sql"), "UTF-8");

        Connection connection = DriverManager.getConnection("jdbc:mysql://172.16.72.147:3310/drools_rules", "root", "123456");

        String sql = "insert into t_canal_rules(`rule_name`,`rule_code`,`rule_status`,`rule_type`,`rule_version`,`cnt_sqls`,`seq_sqls`,`rule_creator`,`rule_auditor`,`create_time`,`update_time`) " +
                "values (?,?,?,?,?,?,?,?,?,Now(),Now())";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "rule1");
        ps.setString(2, rule);
        ps.setInt(3, 1);
        ps.setString(4, "1");
        ps.setString(5, "1");
        ps.setString(6, cntSql);
        ps.setString(7, seqSql);
        ps.setString(8, "fwmagic");
        ps.setString(9, "fwmagic");
        ps.execute();

        ps.close();
        connection.close();
        System.out.println("插入数据成功！");
    }
}
