package com.fwmagic.dynamic_rule.datagen;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.File;
import java.util.Properties;

/**
 * 读取drl文件写入Kafka中
 */
public class ReadFile2Kafka4Drools {
    public static void main(String[] args) throws Exception {
        Properties props = new Properties();
        props.put("bootstrap.servers", "hd1:9092,hd2:9092,hd3:9092");
        //acks=0 配置适用于实现非常高的吞吐量 , acks=all 这是最安全的模式
        props.put("acks", "all");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        //读取文件中的规则
        String path = "/Users/fangwei/workspace/self/fwmagic-yinnew-marketing/dynamic-rule-engine/src/main/resources/rules/flink.drl";
        String rule = FileUtils.readFileToString(new File(path), "UTF-8");
        KafkaProducer producer = new KafkaProducer(props);
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("test-drools-rules", "test1-rule," + rule);
        producer.send(record);
        producer.close();
    }
}
