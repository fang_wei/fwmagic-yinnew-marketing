package com.fwmagic.dynamic_rule.datagen;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能：模拟器生成用户画像数据写入Hbase
 */
public class UserProfileDataGen {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("hbase.zookeeper.quorum", "hd1:2181,hd2:2181,hd3:2181");
        Connection connection = ConnectionFactory.createConnection(conf);
        Table table = connection.getTable(TableName.valueOf("yinew_profile_new"));
        List<Put> puts = new ArrayList<>();
        for (int i = 1; i < 1000000; i++) {
            String deviceId = StringUtils.leftPad(i + "", 6, "0");
            Put put = new Put(deviceId.getBytes());
            //每个人100个属性，即100个k-v对
            for (int j = 1; j < 100; j++) {
                String key = "k" + j;
//                String value = "v" + RandomUtils.nextInt(1, 1000);
                String value = "v" + RandomUtils.nextInt(1, 3);
                put.addColumn("f".getBytes(), key.getBytes(), value.getBytes());
            }
            puts.add(put);
            if (puts.size() == 1000) {
                table.put(puts);
                puts.clear();
            }
//            Thread.sleep(500);
        }
        connection.close();
        System.out.println("插入hbase数据结束！");
    }
}
