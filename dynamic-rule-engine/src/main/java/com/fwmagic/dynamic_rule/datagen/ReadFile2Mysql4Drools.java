package com.fwmagic.dynamic_rule.datagen;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Properties;

/**
 * 读取drl文件写入Mysql中
 */
public class ReadFile2Mysql4Drools {
    public static void main(String[] args) throws Exception {
        //读取文件中的规则
        String path = "/Users/fangwei/workspace/self/fwmagic-yinnew-marketing/dynamic-rule-engine/src/main/resources/rules/flink.drl";
        String rule = FileUtils.readFileToString(new File(path), "UTF-8");

        Connection connection = DriverManager.getConnection("jdbc:mysql://hd1:3306/drools_test", "root", "123456");

        String sql = "insert into t_rules_test(`rule_name`,`rule_code`) values (?,?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "rule-test");
        ps.setString(2, rule);

        ps.execute();

        ps.close();
        connection.close();
    }
}
