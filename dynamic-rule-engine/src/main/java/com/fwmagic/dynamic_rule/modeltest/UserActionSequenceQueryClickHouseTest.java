package com.fwmagic.dynamic_rule.modeltest;

import com.fwmagic.dynamic_rule.bean.RuleAtomicParam;
import com.fwmagic.dynamic_rule.bean.RuleParam;
import com.fwmagic.dynamic_rule.service.impl.UserActionSequenceQueryServiceClickHouseImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class UserActionSequenceQueryClickHouseTest {
    @Test
    public void test() throws Exception {
        UserActionSequenceQueryServiceClickHouseImpl service = new UserActionSequenceQueryServiceClickHouseImpl();

        RuleParam ruleParam = new RuleParam();
        ruleParam.setActionSequenceQuerySql("select \n" +
                "deviceId,\n" +
                "sequenceMatch('.*(?1).*(?2).*(?3)') (\n" +
                "toDateTime(`timeStamp`),\n" +
                "eventId='U' and properties['p5']='v8',\n" +
                "eventId='A' and properties['p1']='v1',\n" +
                "eventId='H' and properties['p2']='v9'\n" +
                ") as isMatch3,\n" +
                "sequenceMatch('.*(?1).*(?2)') (\n" +
                "toDateTime(`timeStamp`),\n" +
                "eventId='U' and properties['p5']='v8',\n" +
                "eventId='A' and properties['p1']='v1',\n" +
                "eventId='H' and properties['p2']='v9'\n" +
                ") as isMatch2,\n" +
                "sequenceMatch('.*(?1).*') (\n" +
                "toDateTime(`timeStamp`),\n" +
                "eventId='U' and properties['p5']='v8',\n" +
                "eventId='A' and properties['p1']='v1',\n" +
                "eventId='H' and properties['p2']='v9'\n" +
                ") as isMatch1\n" +
                "from \n" +
                "yinew_event_detail\n" +
                "where deviceId='000001'\n" +
                "and timeStamp>=0 and timeStamp<2724949257751\n" +
                "and ((eventId='U' and properties['p5']='v8') or (eventId='A' and properties['p1']='v1') or (eventId='H' and properties['p2']='v9'))\n" +
                "group by deviceId");

        //ruleAtomicParams
        ArrayList<RuleAtomicParam> ruleAtomicParams = new ArrayList<>();

        RuleAtomicParam rap1 = new RuleAtomicParam();
        rap1.setEventId("U");
        HashMap<String, String> p1 = new HashMap<>();
        p1.put("p5", "v8");
        rap1.setProperties(p1);
        ruleAtomicParams.add(rap1);

        RuleAtomicParam rap2 = new RuleAtomicParam();
        rap2.setEventId("A");
        HashMap<String, String> p2 = new HashMap<>();
        p2.put("p1", "v1");
        rap2.setProperties(p2);
        ruleAtomicParams.add(rap2);

        RuleAtomicParam rap3 = new RuleAtomicParam();
        rap3.setEventId("H");
        HashMap<String, String> p3 = new HashMap<>();
        p3.put("p2", "v9");
        rap3.setProperties(p3);
        ruleAtomicParams.add(rap3);

        ruleParam.setUserActionSequenceParam(ruleAtomicParams);
        boolean b = service.queryActionSequence("000001", ruleParam);
        System.out.println(b);
    }
}
