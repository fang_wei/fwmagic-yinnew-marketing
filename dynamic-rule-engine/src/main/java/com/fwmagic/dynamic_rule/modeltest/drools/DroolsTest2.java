package com.fwmagic.dynamic_rule.modeltest.drools;

import org.apache.commons.io.FileUtils;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;

import java.io.File;

public class DroolsTest2 {
    public static void main(String[] args) throws Exception {
        KieHelper kieHelper = new KieHelper();

        String drlFilePath = "/Users/fangwei/workspace/self/fwmagic-yinnew-marketing/dynamic-rule-engine/src/main/resources/rules/test.drl";
        String str = FileUtils.readFileToString(new File(drlFilePath), "UTF-8");
        kieHelper.addContent(str, ResourceType.DRL);
        KieSession kieSession = kieHelper.build().newKieSession();

        Applicant applicant = new Applicant("张三", 13);
        kieSession.insert(applicant);
        kieSession.fireAllRules();
    }
}
