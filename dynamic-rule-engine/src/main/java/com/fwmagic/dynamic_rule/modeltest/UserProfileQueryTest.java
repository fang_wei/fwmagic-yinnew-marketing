package com.fwmagic.dynamic_rule.modeltest;

import com.fwmagic.dynamic_rule.bean.RuleParam;
import com.fwmagic.dynamic_rule.service.UserProfileQueryService;
import com.fwmagic.dynamic_rule.service.impl.UserProfileQueryServiceHbaseImpl;
import org.junit.Test;

import java.util.HashMap;

public class UserProfileQueryTest {

    @Test
    public void testJudgeProfileCondition() throws Exception {
        UserProfileQueryService userProfileQueryService = new UserProfileQueryServiceHbaseImpl();
        String deviceId = "000022";
        RuleParam ruleParam = new RuleParam();
        HashMap<String, String> userProfileParam = new HashMap<>();
        userProfileParam.put("k2", "v903");
        userProfileParam.put("k33", "v357");
        userProfileParam.put("k66", "v120");
//        userProfileParam.put("k2", "v22");
//        userProfileParam.put("k33", "v33");
//        userProfileParam.put("k66", "v66");
        ruleParam.setUserProfileParam(userProfileParam);
        boolean condition = userProfileQueryService.judgeProfileCondition(deviceId, ruleParam);
        System.out.println(condition);
    }
}
