package com.fwmagic.dynamic_rule.modeltest.drools;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Applicant {
    private String name;

    private int age;

    private boolean valid;


    public Applicant(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
