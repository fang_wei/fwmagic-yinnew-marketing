package com.fwmagic.dynamic_rule.modeltest.drools;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Action {
    private String msg;

    public void doSomething() {
        System.out.println(msg);
    }
}
