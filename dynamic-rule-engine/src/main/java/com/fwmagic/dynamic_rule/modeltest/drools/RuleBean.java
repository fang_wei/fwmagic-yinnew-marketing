package com.fwmagic.dynamic_rule.modeltest.drools;

import lombok.Data;

import java.util.List;

@Data
public class RuleBean {
    private List<Rule> data;

    private String type;
}

@Data
class Rule {
    private Integer id;

    private String ruleName;

    private String ruleCode;
}