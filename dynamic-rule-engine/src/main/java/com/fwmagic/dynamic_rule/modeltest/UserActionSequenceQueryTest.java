package com.fwmagic.dynamic_rule.modeltest;

import com.fwmagic.dynamic_rule.bean.LogBean;
import com.fwmagic.dynamic_rule.bean.RuleAtomicParam;
import com.fwmagic.dynamic_rule.service.impl.UserActionSequenceQueryServiceStateImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class UserActionSequenceQueryTest {

    @Test
    public void testQueryActionSequence(){
        UserActionSequenceQueryServiceStateImpl queryServiceState = new UserActionSequenceQueryServiceStateImpl(null);

        //logBeans
        ArrayList<LogBean> logBeans = new ArrayList<>();

        LogBean logBean1 = new LogBean();
        logBean1.setEventId("A");
        HashMap<String, String> properties1 = new HashMap<>();
        properties1.put("p1", "v9");
        logBean1.setProperties(properties1);
        logBeans.add(logBean1);

        LogBean logBean2 = new LogBean();
        logBean2.setEventId("B");
        HashMap<String, String> properties2 = new HashMap<>();
        properties2.put("p2", "v8");
        logBean2.setProperties(properties2);
        logBeans.add(logBean2);

        LogBean logBean3 = new LogBean();
        logBean3.setEventId("E");
        HashMap<String, String> properties3 = new HashMap<>();
        properties3.put("p1", "v9");
        logBean3.setProperties(properties3);
        logBeans.add(logBean3);

        LogBean logBean4 = new LogBean();
        logBean4.setEventId("C");
        HashMap<String, String> properties4 = new HashMap<>();
        properties4.put("p4", "v5");
        logBean4.setProperties(properties4);
        logBeans.add(logBean4);


        //ruleAtomicParams
        ArrayList<RuleAtomicParam> ruleAtomicParams = new ArrayList<>();

        RuleAtomicParam rap1 = new RuleAtomicParam();
        rap1.setEventId("A");
        HashMap<String, String> p1 = new HashMap<>();
        p1.put("p1", "v9");
        rap1.setProperties(p1);
        ruleAtomicParams.add(rap1);

        RuleAtomicParam rap2 = new RuleAtomicParam();
        rap2.setEventId("B");
        HashMap<String, String> p2 = new HashMap<>();
        p2.put("p2", "v8");
        rap2.setProperties(p2);
        ruleAtomicParams.add(rap2);

        RuleAtomicParam rap3 = new RuleAtomicParam();
        rap3.setEventId("C");
        HashMap<String, String> p3 = new HashMap<>();
        p3.put("p4", "v5");
        rap3.setProperties(p3);
        ruleAtomicParams.add(rap3);

        //查询步骤数
        int maxStep = queryServiceState.queryActionSequenceHelperV1(logBeans, ruleAtomicParams);
        System.out.println("maxStep:"+maxStep);
    }
}
