package com.fwmagic.dynamic_rule.modeltest;

import com.fwmagic.dynamic_rule.bean.RuleAtomicParam;
import com.fwmagic.dynamic_rule.bean.RuleParam;
import com.fwmagic.dynamic_rule.service.impl.UserActionCountQueryServiceClickHouseImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class UserActionCountQueryClickHouseTest {
    @Test
    public void test() throws Exception {
        UserActionCountQueryServiceClickHouseImpl service = new UserActionCountQueryServiceClickHouseImpl();
        RuleParam ruleParam = new RuleParam();

        ArrayList<RuleAtomicParam> ruleAtomicParams = new ArrayList<>();

        RuleAtomicParam rap1 = new RuleAtomicParam();
        rap1.setEventId("D");
        rap1.setCnt(2);
        HashMap<String, String> p1 = new HashMap<>();
        p1.put("p1", "v9");
        rap1.setProperties(p1);
        rap1.setActionCountQuerySql("select deviceId,count(1) cnt\n" +
                "from \n" +
                "yinew_event_detail\n" +
                "where deviceId='000001' and eventId='D'\n" +
                "and timeStamp>0 and timeStamp<1724949257751\n" +
                "and properties['p1']='v9' \n" +
                "group by deviceId");
        ruleAtomicParams.add(rap1);

        RuleAtomicParam rap2 = new RuleAtomicParam();
        rap2.setEventId("Z");
        rap2.setCnt(1);
        HashMap<String, String> p2 = new HashMap<>();
        p2.put("p8", "v2");
        rap2.setProperties(p2);
        rap2.setActionCountQuerySql("select deviceId,count(1) cnt\n" +
                "from \n" +
                "yinew_event_detail\n" +
                "where deviceId='000001' and eventId='Z'\n" +
                "and timeStamp>0 and timeStamp<1724949257751\n" +
                "and properties['p8']='v2' \n" +
                "group by deviceId");
        ruleAtomicParams.add(rap2);

        ruleParam.setUserActionCountParam(ruleAtomicParams);

        boolean b = service.queryActionCounts("000001", ruleParam);
        System.out.println(b);
    }
}
