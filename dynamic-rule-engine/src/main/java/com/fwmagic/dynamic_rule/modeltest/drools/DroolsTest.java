package com.fwmagic.dynamic_rule.modeltest.drools;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class DroolsTest {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.Factory.get();
        //默认自动加载 META/INF/kmodule.xml
        KieContainer classpathContainer = kieServices.getKieClasspathContainer();
        //kmodule.xml中定义的ksession name
        KieSession kieSession = classpathContainer.newKieSession("all-rules");
        Applicant applicant = new Applicant("Mr John Smith", 17);
        kieSession.insert(applicant);
        kieSession.fireAllRules();
        kieSession.dispose();
    }
}
