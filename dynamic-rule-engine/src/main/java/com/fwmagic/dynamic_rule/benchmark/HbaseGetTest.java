package com.fwmagic.dynamic_rule.benchmark;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseGetTest {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        conf.set("hbase.zookeeper.quorum", "hd1:2181,hd2:2181,hd3:2181");
        Connection connection = ConnectionFactory.createConnection(conf);
        Table table = connection.getTable(TableName.valueOf("yinew_profile_new"));

        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            String deviceId = StringUtils.leftPad(i + "", 6, "0");
            Get get = new Get(deviceId.getBytes());
            int i1 = RandomUtils.nextInt(1, 100);
            int i2 = RandomUtils.nextInt(1, 100);
            int i3 = RandomUtils.nextInt(1, 100);
            get.addColumn("f".getBytes(), Bytes.toBytes("k" + i1));
            get.addColumn("f".getBytes(), Bytes.toBytes("k" + i2));
            get.addColumn("f".getBytes(), Bytes.toBytes("k" + i3));
            Result result = table.get(get);
            result.getValue("f".getBytes(), Bytes.toBytes("k" + i1));
            result.getValue("f".getBytes(), Bytes.toBytes("k" + i2));
            result.getValue("f".getBytes(), Bytes.toBytes("k" + i3));
        }
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start) + "ms");
        connection.close();
    }
}
