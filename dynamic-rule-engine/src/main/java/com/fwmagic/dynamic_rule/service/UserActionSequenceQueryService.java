package com.fwmagic.dynamic_rule.service;

import com.fwmagic.dynamic_rule.bean.RuleParam;

/**
 * 用户行为次数序列查询服务接口
 */
public interface UserActionSequenceQueryService {
    boolean queryActionSequence(String deviceId,RuleParam ruleParam) throws Exception;
}
