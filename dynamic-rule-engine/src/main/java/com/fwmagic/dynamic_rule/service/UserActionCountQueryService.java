package com.fwmagic.dynamic_rule.service;

import com.fwmagic.dynamic_rule.bean.RuleAtomicParam;
import com.fwmagic.dynamic_rule.bean.RuleParam;

/**
 * 查询用户行为次数条件查询服务
 */
public interface UserActionCountQueryService {

    boolean queryActionCounts(String deviceId, RuleParam ruleParam) throws Exception;

    boolean queryActionCounts(String deviceId, RuleAtomicParam ruleAtomicParam) throws Exception;
}
