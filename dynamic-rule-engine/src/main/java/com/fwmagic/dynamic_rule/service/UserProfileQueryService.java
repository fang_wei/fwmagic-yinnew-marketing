package com.fwmagic.dynamic_rule.service;

import com.fwmagic.dynamic_rule.bean.RuleParam;

/**
 * 用户画像查询服务
 */
public interface UserProfileQueryService {

    boolean judgeProfileCondition(String deviceId, RuleParam ruleParam);
}
