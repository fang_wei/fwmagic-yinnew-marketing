package com.fwmagic.dynamic_rule.functions;

import com.alibaba.fastjson.JSON;
import com.fwmagic.dynamic_rule.bean.LogBean;
import org.apache.flink.api.common.functions.MapFunction;

public class Json2BeanMapFunction implements MapFunction<String, LogBean> {
    @Override
    public LogBean map(String json) throws Exception {
        return JSON.parseObject(json, LogBean.class);
    }
}
