package com.fwmagic.dynamic_rule.functions;

import com.fwmagic.dynamic_rule.bean.LogBean;
import org.apache.flink.api.java.functions.KeySelector;

public class DeviceIdKeySelector implements KeySelector<LogBean, String> {
    @Override
    public String getKey(LogBean logBean) throws Exception {
        return logBean.getDeviceId();
    }
}
