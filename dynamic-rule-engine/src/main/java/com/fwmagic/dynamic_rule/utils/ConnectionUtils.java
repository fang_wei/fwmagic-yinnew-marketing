package com.fwmagic.dynamic_rule.utils;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;

@Slf4j
public class ConnectionUtils {
    /**
     * 获取ClickHouse连接
     *
     * @return
     * @throws Exception
     */
    public static Connection getClickHouseConnection() throws Exception {
        Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
        Connection connection = DriverManager.getConnection("jdbc:clickhouse://172.16.72.147:8123");
        log.debug("ClickHouse连接创建完成！");
        return connection;
    }
}
