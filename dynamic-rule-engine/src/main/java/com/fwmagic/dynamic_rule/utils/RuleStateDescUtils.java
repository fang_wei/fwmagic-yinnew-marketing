package com.fwmagic.dynamic_rule.utils;

import com.fwmagic.dynamic_rule.bean.LogBean;
import com.fwmagic.dynamic_rule.bean.RuleStateBean;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.kie.api.runtime.KieSession;

/**
 * 状态描述器
 */
public class RuleStateDescUtils {

    public static final MapStateDescriptor<String, RuleStateBean> ruleKieStateDesc = new MapStateDescriptor("ruleKieState", String.class, RuleStateBean.class);


    public static final ListStateDescriptor<LogBean> eventsStateDesc = new ListStateDescriptor<>("eventsStateDesc", LogBean.class);

}
