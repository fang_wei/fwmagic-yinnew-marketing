package com.fwmagic.dynamic_rule.utils;

import com.fwmagic.dynamic_rule.bean.LogBean;
import com.fwmagic.dynamic_rule.bean.RuleAtomicParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RuleCalcUtils {

    /**
     * 用户可指定是否需要根据时间匹配事件
     *
     * @param logBean
     * @param ruleAtomicParam
     * @param needTimeComapre
     * @return
     */
    public static boolean eventBeanParamMatchRuleParam(LogBean logBean, RuleAtomicParam ruleAtomicParam, boolean needTimeComapre) {
        if (needTimeComapre) {
            long timeStamp = logBean.getTimeStamp();
            long start = ruleAtomicParam.getRangeStart();
            long end = ruleAtomicParam.getRangeEnd();
            boolean startTimeMatch = timeStamp >= (start == -1 ? Long.MIN_VALUE : start);
            boolean endTimeMatch = timeStamp <= (end == -1 ? Long.MAX_VALUE : end);
            boolean ruleMatch = eventBeanParamMatchRuleParam(logBean, ruleAtomicParam);
            return startTimeMatch && endTimeMatch && ruleMatch;
        } else {
            return eventBeanParamMatchRuleParam(logBean, ruleAtomicParam);
        }
    }

    /**
     * 功能：事件匹配规则
     *
     * @param logBean
     * @param ruleAtomicParam
     * @return
     */
    public static boolean eventBeanParamMatchRuleParam(LogBean logBean, RuleAtomicParam ruleAtomicParam) {
        //事件id匹配
        if (logBean.getEventId().equals(ruleAtomicParam.getEventId())) {
            //事件属性参数
            Map<String, String> eventParams = logBean.getProperties();
            //规则属性参数
            HashMap<String, String> ruleParams = ruleAtomicParam.getProperties();
            //属性匹配
            for (Map.Entry<String, String> entry : ruleParams.entrySet()) {
                if (!(entry.getValue().equals(eventParams.get(entry.getKey())))) {
                    return false;
                }
            }
            //都匹配
            return true;
        }
        return false;
    }

    /**
     * count 获取缓存所需的Key
     *
     * @param ruleAtomicParam
     * @return deviceId-event-p1-v1-p2-v2
     */
    public static String getCacheKey(String deviceId, RuleAtomicParam ruleAtomicParam) {
        StringBuffer sb = new StringBuffer();
        sb.append(deviceId).append("-").append(ruleAtomicParam.getEventId());
        appendProperties(sb, ruleAtomicParam);
        return sb.toString();
    }

    /**
     * sequence 获取缓存所需的Key
     *
     * @param deviceId
     * @param userActionSequenceParam
     * @return deviceId-eventId1-p1-v1-p2-v2-eventId2-p1-v1-eventId
     */
    public static String getCacheKey(String deviceId, List<RuleAtomicParam> userActionSequenceParam) {
        StringBuffer sb = new StringBuffer();
        sb.append(deviceId);

        for (RuleAtomicParam ruleAtomicParam : userActionSequenceParam) {
            sb.append("-").append(ruleAtomicParam.getEventId());
            appendProperties(sb, ruleAtomicParam);
        }
        return sb.toString();
    }

    private static void appendProperties(StringBuffer sb, RuleAtomicParam ruleAtomicParam) {
        HashMap<String, String> properties = ruleAtomicParam.getProperties();
        Set<Map.Entry<String, String>> entries = properties.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            sb.append("-").append(entry.getKey()).append("-").append(entry.getValue());
        }
    }

}
