package com.fwmagic.dynamic_rule.utils;

import com.fwmagic.dynamic_rule.bean.RuleAtomicParam;
import com.fwmagic.dynamic_rule.bean.RuleParam;

import java.util.ArrayList;
import java.util.HashMap;

public class RuleSimulator {

//    private static final Long rangeStart = 1625814025000L;
    private static final Long rangeStart = 0L;
    private static final Long rangeEnd = Long.MAX_VALUE;
//    private static final Long rangeEnd = 1625817325000L;


    /**
     * 场景：
     * 时间:startTime:0,endTime:Long.MAX_VALUE
     * 1、触发条件：E事件
     * 2、画像条件:k1=v1
     * 3、事件发生的次序条件：D(p6=v4) -> F(p1=v3)
     * 4、事件发生次数条件：A(p1=v4,p7=v7) >=2次，B(p3=v1) >=2次
     */
    public static RuleParam getRuleParam() {
        RuleParam ruleParam = new RuleParam();
        ruleParam.setRuleId("rule_test_666");
        //1、构造触发条件
        RuleAtomicParam triggerParam = new RuleAtomicParam();
        triggerParam.setEventId("E");
        ruleParam.setTriggerParam(triggerParam);

        //2、构造画像条件
        HashMap<String, String> userProfileParam = new HashMap<>();
        userProfileParam.put("k1", "v1");
        ruleParam.setUserProfileParam(userProfileParam);


        //3、构造事件发生的次序条件
        ruleParam.setActionSequenceQuerySql("select \n" +
                "deviceId,\n" +
                "sequenceMatch('.*(?1).*(?2)') (\n" +
                "toDateTime(`timeStamp`),\n" +
                "eventId='D' and properties['p6']='v4',\n" +
                "eventId='F' and properties['p1']='v3'\n" +
                ") as isMatch2,\n" +
                "sequenceMatch('.*(?1).*') (\n" +
                "toDateTime(`timeStamp`),\n" +
                "eventId='D' and properties['p6']='v4',\n" +
                "eventId='F' and properties['p1']='v3'\n" +
                ") as isMatch1\n" +
                "from \n" +
                "yinew_event_detail\n" +
                "where deviceId='${deviceId}'\n" +
                "and timeStamp>=0 and timeStamp<2724949257751\n" +
                "and ((eventId='D' and properties['p6']='v4') or (eventId='F' and properties['p1']='v3'))\n" +
                "group by deviceId");

        //ruleAtomicParams
        ArrayList<RuleAtomicParam> sequenceRuleAtomicParams = new ArrayList<>();

        RuleAtomicParam rap = new RuleAtomicParam();
        rap.setEventId("D");
        HashMap<String, String> p = new HashMap<>();
        p.put("p6", "v4");
        rap.setProperties(p);

        rap.setOriginRangeStart(rangeStart);
        rap.setOriginRangeEnd(rangeEnd);
        sequenceRuleAtomicParams.add(rap);

        RuleAtomicParam rap22 = new RuleAtomicParam();
        rap22.setEventId("F");
        HashMap<String, String> p22 = new HashMap<>();
        p22.put("p1", "v3");
        rap22.setProperties(p22);
        rap22.setOriginRangeStart(rangeStart);
        rap22.setOriginRangeEnd(rangeEnd);
        sequenceRuleAtomicParams.add(rap22);

        ruleParam.setUserActionSequenceParam(sequenceRuleAtomicParams);

        //4、构造时间发生次数条件
        //A(p1=v4,p7=v7) >=2次，B(p3=v1) >=2次
        ArrayList<RuleAtomicParam> ruleAtomicParams = new ArrayList<>();

        RuleAtomicParam rap1 = new RuleAtomicParam();
        rap1.setEventId("A");
        rap1.setCnt(2);
        HashMap<String, String> p1 = new HashMap<>();
        p1.put("p1", "v4");
        p1.put("p7", "v7");
        rap1.setProperties(p1);
        rap1.setOriginRangeStart(rangeStart);
        rap1.setOriginRangeEnd(rangeEnd);
        rap1.setActionCountQuerySql("select deviceId,count(1) cnt\n" +
                "from \n" +
                "yinew_event_detail\n" +
                "where deviceId='${deviceId}' and eventId='A'\n" +
                "and timeStamp>0 and timeStamp<2724949257751\n" +
                "and properties['p1']='v4' \n" +
                "and properties['p7']='v7' \n" +
                "group by deviceId");
        ruleAtomicParams.add(rap1);

        RuleAtomicParam rap2 = new RuleAtomicParam();
        rap2.setEventId("B");
        rap2.setCnt(2);
        HashMap<String, String> p2 = new HashMap<>();
        p2.put("p3", "v1");
        rap2.setProperties(p2);
        rap2.setOriginRangeStart(rangeStart);
        rap2.setOriginRangeEnd(rangeEnd);
        rap2.setActionCountQuerySql("select deviceId,count(1) cnt\n" +
                "from \n" +
                "yinew_event_detail\n" +
                "where deviceId='${deviceId}' and eventId='B'\n" +
                "and timeStamp>0 and timeStamp<2724949257751\n" +
                "and properties['p3']='v1' \n" +
                "group by deviceId");
        ruleAtomicParams.add(rap2);

        ruleParam.setUserActionCountParam(ruleAtomicParams);


        return ruleParam;
    }
}
