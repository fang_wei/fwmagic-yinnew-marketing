package com.fwmagic.dynamic_rule.utils;

public class SystemPrintUtils {
    /**
     * 打印当前线程的信息
     *
     * @param msg
     */
    public static void printLog(String msg) {
        System.out.println(Thread.currentThread().getName() + " | " + msg);
    }
}
