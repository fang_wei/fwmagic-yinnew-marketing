import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TestClickHouse {
    public static void main(String[] args) throws Exception {
        Class.forName("ru.yandex.clickhouse.ClickHouseDriver");
        Connection connection = DriverManager.getConnection("jdbc:clickhouse://172.16.72.147:8123");

        String sql = "select deviceId,\n" +
                "       sequenceMatch('.*(?1).*(?2).*')(\n" +
                "                     toDateTime(`timeStamp`),\n" +
                "                     eventId = 'A' and properties['p3'] = 'v2',\n" +
                "                     eventId = 'C' and properties['p6'] = 'v2'\n" +
                "           ) as isMatch2,\n" +
                "       sequenceMatch('.*(?1).*')(\n" +
                "                     toDateTime(`timeStamp`),\n" +
                "                     eventId = 'A' and properties['p3'] = 'v2',\n" +
                "                     eventId = 'C' and properties['p6'] = 'v2'\n" +
                "           ) as isMatch1\n" +
                "from yinew_event_detail\n" +
                "where deviceId = ?\n" +
                "  and timeStamp between ? and ?\n" +
                "  and ((eventId = 'A' and properties['p3'] = 'v2') or (eventId = 'C' and properties['p6'] = 'v2'))\n" +
                "group by deviceId;";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1,"000001");
        ps.setLong(2,0);
        ps.setLong(3,2724949257751L);

        System.err.println(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            String deviceId = rs.getString(1);
            int isMatch2 = rs.getInt(2);
            int isMatch1 = rs.getInt(3);
            System.out.println(deviceId+","+isMatch1+","+isMatch2);
        }
    }

    private void test1(Connection connection) throws Exception {
        String sql = "select * from yinew_event_detail limit 5";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String account = rs.getString(1);
            String eventId = rs.getString("eventId");
            String p = rs.getString("properties");
            System.out.println(account + " | " + eventId + " | " + p);
        }
    }
}
